
	$(function(){

		$("#pagination a").trigger('click'); // When page is loaded we trigger a click
	});

	$('#pagination').on('click', 'a', function(e) {
		var page = this.id;
		var pagination = '';
		
		$('#noticias').html('<p><i class="fa fa-spinner fa-spin"></i> Cargando...</p>'); // Display a processing icon
		var data = {page: page, per_page: 10};
		let aux = '';
		let aux2 = '';
		$.ajax({ 
			type: 'POST',
			url: 'pagination.php', 
			data: data, // We send the data string
			dataType: 'json',
			timeout: 3000,
			success: function(data) {
				console.log(data.articleList);
				$.each(data.articleList, function(key, value) {
	        		// cantidad_resultados = result.length - 1;

	        		// console.log(value.id, value.titulo, value.resumen, value.fotoPath, value.primera);
	        		if(value.primera === true && page == 1){

						aux += '<div class="news-featured" id="news'+ value.id +'">';
			      		aux += '<div class="news-img"><img src="'+ value.fotoPath +'" /></div>';
			      		aux += '<div class="news-main">';
			        	aux += '<h1 class="news-title" onclick="openNews('+ value.id +')"><a class="news_readMore">'+ value.titulo +'</a></h1>';
			        	aux += '<div class="news-content">';
			          	aux += '<p>'+ value.resumen +'</p>';
			          	aux += '<p><a class="news_readMore" onclick="openNews('+ value.id +')">Leer más...</a></p>';
			        	aux += '</div></div></div>'; 

	        		}else{
						aux2 += '<div class="news-container" id="news'+ value.id +'">';
				        aux2 += '<div class="news-img"><img src="'+ value.fotoPath +'" /></div>';
				        aux2 += '<div class="news-main">';
				        aux2 += '<h2 class="news-title"><a onclick="openNews('+ value.id +')">'+ value.titulo +'</a></h2>';
				        aux2 += '<div class="news-content">';
				        aux2 += '<p>'+ value.resumen +'</p>';
				        aux2 += '<p><a class="news_readMore" onclick="openNews('+ value.id +')">Leer más...</a></p>';
				        aux2 += '</div></div></div>';
	        		}

	        	});

				$('.aux').html(aux);
				$('.aux2').html(aux2);

				// Paginacion
				if (page == 1) pagination += '<div class="cell_disabled"><span>Primera</span></div><div class="cell_disabled"><span>Ant.</span></div>';
				else pagination += '<div class="cell"><a href="#" id="1">Primera</a></div><div class="cell"><a href="#" id="' + (page - 1) + '">Ant.</span></a></div>';
	 
				for (var i=parseInt(page)-3; i<=parseInt(page)+3; i++) {
					if (i >= 1 && i <= data.numPage) {
						pagination += '<div';
						if (i == page) pagination += ' class="cell_active"><span>' + i + '</span>';
						else pagination += ' class="cell"><a href="#" id="' + i + '">' + i + '</a>';
						pagination += '</div>';
					}
				}
	 
				if (page == data.numPage) pagination += '<div class="cell_disabled"><span>Sig.</span></div><div class="cell_disabled"><span>Última</span></div>';
				else pagination += '<div class="cell"><a href="#" id="' + (parseInt(page) + 1) + '">Sig.</a></div><div class="cell"><a href="#" id="' + data.numPage + '">Última</span></a></div>';
				
				$('#pagination').html(pagination);
			},
			error: function() {
			}
		});
		return false;
	});




	function openNews(id_news){
		// alert('noticia.php?id=' + id_news);
		document.location.href = 'noticia.php?id=' + id_news;
	}

	// Publicacion de noticia form
	var form = $('#noticias-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		var resultado = $("#resultado");
		var form_data = new FormData(this); //Creates new FormData object
		// console.log(form_data);

		$.ajax({
			url: 'altanoticia.php',
			type: "POST",
			data: form_data, //datos que se envian al server
			dataType: 'json',  // what to expect back from the PHP script, if anything
	        cache: false,
	        contentType: false,
	        processData: false,
			beforeSend: function(){
				form.prepend(form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Subiendo Noticia...</p>').fadeIn() );
			}

		}).done(function(response){
			console.log(response);
	     	resultado.html("");
			$.each(response, function(key,value){

				if(value.error===false){
					//output the response message below the form
		            // $("<div class='alert alert-success'>" + value.message + "</div>").appendTo("#resultado");
		            resultado.html("<div class='alert alert-success'>" + value.message + "</div>");
					form_status.html('<p>Noticia Publicada.</p>').delay(3000).fadeOut();

		            //redirect to a page
		            //  window.location.href = "nextpage.php";
		         }else{
		         	$("<div class='alert alert-danger'>" + value.message + "</div>").appendTo("#resultado");
					form_status.html('<p>Problemas al subir la noticia.</p>').delay(3000).fadeOut();
		         }

			})
		});
	});