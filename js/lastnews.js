$(function(){

	// $('#noticias').html('<p><i class="fa fa-spinner fa-spin"></i> Cargando...</p>'); // Display a processing icon
		let aux = '';
		$.ajax({ 
			type: 'GET',
			url: 'noticias/lastnews.php',
			dataType: 'json',
			timeout: 3000,
			success: function(data) {
				console.log(data.articleList);
				$.each(data.articleList, function(key, value) {
	        		// console.log(value.id, value.titulo, value.resumen, value.fotoPath, value.primera);	        		
					aux += '<div class="news-container" id="news'+ value.id +'">';
			        aux += '<div class="news-img"><img src="noticias/'+ value.fotoPath +'" /></div>';
			        aux += '<div class="news-main">';
			        aux += '<h2 class="news-title"><a onclick="openNews('+ value.id +')">'+ value.titulo +'</a></h2>';
			        aux += '<div class="news-content">';
			        aux += '<p>'+ value.resumen +'</p>';
			        aux += '<p onclick="openNews('+ value.id +')"><a class="news_readMore">Leer más...</a></p>';
			        aux += '</div></div></div>';
	        	});

				$('.news-grid').html(aux);

			}
		});

});

function openNews(id_news){
		// alert('noticia.php?id=' + id_news);
		document.location.href = 'noticias/noticia.php?id=' + id_news;
	}