$('document').ready(function(){ 
     /* validation */
  $("#login-form").validate({
      rules:
         {
         password: {
                   required: true,
                   },
         username: {
                  required: true
                  // email: true
                  },
          },
             messages:
                      {
                        password:{
                                  required: "por favor ingrese su password"
                                 },
                        username: "por favor ingrese su nombre de usuario",
                       },
          submitHandler: submitForm 
       });  
    /* validation */
    
    /* login submit */
    function submitForm()
    {  
   var data = $("#login-form").serialize();
    
   $.ajax({
    
   type : 'POST',
   url  : 'login_process.php',
   data : data,
   beforeSend: function()
   { 
    $("#error").fadeOut();
    $("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; enviando...');
   },
   success :  function(response)
      {      
     if(response=="ok"){
         
      $("#btn-login").html('<img src="btn-ajax-loader.gif" /> &nbsp; Ingresando...');
      setTimeout(' window.location.href = "panel.php"; ',1000);
     }
     else{
         
      $("#error").fadeIn(1000, function(){      
    $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
           $("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Ingresar');
         });
     }
     }
   });
    return false;
  }
    /* login submit */
});