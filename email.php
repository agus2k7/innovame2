<?php
// Libreria PHPMailer
require 'PHPMailer/PHPMailerAutoload.php';
 
// Creamos una nueva instancia
$mail = new PHPMailer();
 
// Activamos el servicio SMTP
$mail->isSMTP();
// Activamos / Desactivamos el "debug" de SMTP 
// 0 = Apagado 
// 1 = Mensaje de Cliente 
// 2 = Mensaje de Cliente y Servidor 
$mail->SMTPDebug = 2; 
 
// Log del debug SMTP en formato HTML 
$mail->Debugoutput = 'html'; 
 
// Servidor SMTP (para este ejemplo utilizamos gmail) 
$mail->Host = 'smtp.gmail.com'; 
 
// Puerto SMTP 
$mail->Port = 587; 
 
// Tipo de encriptacion SSL ya no se utiliza se recomienda TSL 
$mail->SMTPSecure = 'tls'; 
 
// Si necesitamos autentificarnos 
$mail->SMTPAuth = true; 
 
// Usuario del correo desde el cual queremos enviar, para Gmail recordar usar el usuario completo (usuario@gmail.com) 
$mail->Username = "innova@mec.gob.ar"; //Correo de gmail
 
// Contraseña 
$mail->Password = ""; //Pass de gmail
 



/* Conectamos a la base de datos 
$db = new mysqli('hostname', 'usuario', 'cotraseña', 'basededatos'); 
 
if ($db->connect_errno > 0) { 
    die('Imposible conectar [' . $db->connect_error . ']'); 
} 
 
// Creamos la sentencias SQL 
$result = $db->query("SELECT * FROM personas");
*/ 

// Iniciamos el "bucle" para enviar multiples correos. 
 
//while($row = $result->fetch_assoc()) { 
    //Añadimos la direccion de quien envia el corre, en este caso Codejobs, primero el correo, luego el nombre de quien lo envia. 

    $name       = $_POST['name']; 
    $from       = $_POST['email']; 
    $subject    = $_POST['subject']; 
    $message    = $_POST['message']; 
    $to         = 'innova@mec.gob.ar';//mail de innovame

 
 
    $mail->setFrom($to, $name);
    $mail->addAddress($to, 'Innovame'); 


 
    //La linea de asunto 
    $mail->Subject = $subject; 
 
    // La mejor forma de enviar un correo, es creando un HTML e insertandolo de la siguiente forma, PHPMailer permite insertar, imagenes, css, etc. (No se recomienda el uso de Javascript) 
 
    //html con formato para el mail
    // $mail->msgHTML(file_get_contents('contenido.html'), dirname(__FILE__)); 
    $mail->msgHTML('Mensaje enviado a través del sitio de innovame: <br>
        <p>'.$message .'</p><br><br>comunicate con este usuario a través de este mail: '. $from); 

    //$mail->AddAttachment($destino, $_FILES["archivo_fls"]["name"]);
    //$mail->AddAttachment("imagen.jpg");
    // Enviamos el Mensaje 
    
    if(!$mail->send()){

    $var = "Error: ".$mail->ErrorInfo;
    }else{
        echo "Envio realizado";
        echo '<script>console.log("error "+$var);</script>';
    }
 
 
    // Borramos el destinatario, de esta forma nuestros clientes no ven los correos de las otras personas y parece que fuera un único correo para ellos. 
    $mail->ClearAddresses(); 
//}  
?>