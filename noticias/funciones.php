<?php 
include('conexion.php');
ob_start();
if (isset($_GET['id'])){
	listNews($_GET['id']);
} else {
	listAllNews();
}


function listAllNews(){
	$primera = true;
	$aux = '';
	$rows = array();
	$conection = conectar();

	$sql = "SELECT * FROM noticia where activo = 1 ORDER BY id desc;";
	$statement = $conection->prepare($sql);
	$statement->execute();
	// $row = $sth->fetchAll();

	 while( $row = $statement->fetch()) {
	 	if($primera){
	 		array_push($rows, array('id' => $row['id'],
						     	'titulo' => $row['titulo'],
						     	'resumen' => $row['resumen'],
						     	'fotoPath' => $row['fotoPath'],
						     	'primera' => true
						     	)
		     );
	 		$primera = false;
	 	}else{
	 		array_push($rows, array('id' => $row['id'],
						     	'titulo' => $row['titulo'],
						     	'resumen' => $row['resumen'],
						     	'fotoPath' => $row['fotoPath'],
						     	'primera' => false
						     	)
		     );
	 	}
	     
	}

    // header('Content-Type: application/json');
    echo json_encode($rows);
}

function listNews($id_news){
	$conection = conectar();
	$sql = "SET lc_time_names = 'es_AR'";
	$conection->query($sql);
	// mysqli_query($conection,$query);
	
	$sql = "SELECT titulo, fotoPath, resumen, contenido, DATE_FORMAT(fechaSubida, '%d %M %Y') as fechaSubida FROM noticia WHERE id = :id_news LIMIT 1;";
	$sth = $conection->prepare($sql);
	$sth->execute(array(':id_news' => $id_news));
	$row = $sth->fetchAll();

	// $result = $conn->query($sql);

	// mysqli_set_charset($conection,"utf8"); // esto anda!
	// $result = mysqli_query($conection,$query);
	
	// if ($result == null){echo 'Esta vacio'; die();}
	// $row = mysqli_fetch_array($result);

	// mysqli_free_result($result);
	// desconectar($conection);
	return $row[0];
}

 ?>