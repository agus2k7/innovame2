<?php
include('conexion.php'); // Connection to database
 
if($_POST) {
	$page = $_POST['page']; // Current page number
	$per_page = $_POST['per_page']; // Articles per page
	if ($page != 1) $start = ($page-1) * $per_page;
	else $start=0;
	$conection = conectar();


	$select = $conection->query('SELECT * FROM noticia WHERE activo = 1 ORDER BY id DESC LIMIT '.$start.', '.$per_page.''); // Select article list from $start
	$select->setFetchMode(PDO::FETCH_OBJ);
	$numArticles = $conection->query('SELECT count(id) FROM noticia')->fetch(PDO::FETCH_NUM); // Total number of articles in the database
	
	$numPage = ceil($numArticles[0] / $per_page); // Total number of page
	
	$primera = true;
	$rows = array();

	while( $row = $select->fetch()) {
	 	if($primera){
	 		array_push($rows, array('id' => $row->id,
						     	'titulo' => $row->titulo,
						     	'resumen' => $row->resumen,
						     	'fotoPath' => $row->fotoPath,
						     	'primera' => true
						     	)
		     );
	 		$primera = false;
	 	}else{
	 		array_push($rows, array('id' => $row->id,
						     	'titulo' => $row->titulo,
						     	'resumen' => $row->resumen,
						     	'fotoPath' => $row->fotoPath,
						     	'primera' => false
						     	)
		     );
	 	}
	     
	}
	
	// We send back the total number of page and the article list
	$dataBack = array('numPage' => $numPage, 'articleList' => $rows);
	// header('Content-Type: application/json');
	$dataBack = json_encode($dataBack);
	
	echo $dataBack;
}
?>