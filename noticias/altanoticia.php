<?php
// require 'conexion.php';
header('Content-Type: application/json');
include('conexion.php');
require_once  "bulletproof.php";


// $error = false;
 $min_titulo = 10;
 // $max_titulo = 200;
 $min_resumen = 100;
 $max_resumen = 350;
 $min_contenido = 400;




	if (isset($_POST['titulo'])){
		$titulo = $_POST['titulo'];
	}


	if (isset($_POST['resumen'])){
		$resumen = $_POST['resumen'];
	}

	if (isset($_POST['contenido'])){
		$contenido = $_POST['contenido'];
	}

	if (isset($_POST['user_id'])){
		$user_id = $_POST['user_id'];
	}

	// print json_encode(array('type'=>'error', 'text' => $_FILES));
	// print json_encode(array('type'=>'error', 'text' => $_FILES['pictures']));



	//Validaciones
	$response = array();



	$length_titulo = strlen($titulo);
	$length_resumen = strlen($resumen);
	$length_contenido = strlen($contenido);


	// if($length_titulo < $min_titulo || $length_titulo >= $max_titulo)
	if($length_titulo < $min_titulo){
	    $response[] = array(
		                "error" => true,
		                "message" => "debe agregar un titulo a la noticia mayor a ".$min_titulo." caracteres");
	}

	if($length_resumen < $min_resumen || $length_resumen >= $max_resumen){
	    $response[] = array(
		                "error" => true,
		                "message" => "debe agregar un resumen a la noticia mayor a ".$min_resumen." caracteres y menor a ".$max_resumen);
	}

	if(strlen($contenido) < $min_contenido){
	    $response[] = array(
		                "error" => true,
		                "message" => "debe agregar un contenido a la noticia mayor a ".$min_contenido." caracteres");
	}


// echo $titulo;
// echo $resumen;
// echo $contenido;
// 	echo json_encode($titulo);
// die;


	if (count($response) > 0){
		print (json_encode($response));
		die;
	}else{
		$image = new Bulletproof\Image($_FILES);
		// if($image["pictures"]){
		$folderName = 'img';
		$image['pictures'];
		// pass name (and optional chmod) to create folder for storage
		$image->setLocation($folderName);  
	    $upload = $image->upload(); 
		
	    if($upload){
			$path = $upload->getFullPath();
	    }else{
	        $path = $image["error"]; 
	    }
		// }
		AddNews($titulo, $path, $resumen, $contenido,$user_id);


		$response[] = array(
		                "error" => false,
		                "message" => "noticia agregada correctamente");
		print (json_encode($response));
	}


function addNews($titulo,$pathFoto,$resumen,$contenido,$user_id){
	$link = conectar();
	$statement = $link->prepare("INSERT INTO noticia(titulo, fotoPath, resumen, contenido, user_id)
	    VALUES(:titu, :fotop, :res, :cont, :uid)");
	$statement->execute(array(
	    "titu" => $titulo,
	    "fotop" => $pathFoto,
	    "res" => $resumen,
	    "cont" => $contenido,
	    "uid" => $user_id
	));

}


?>