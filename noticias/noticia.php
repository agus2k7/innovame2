<?php 
  include('funciones.php');ob_start();
  if (isset($_GET['id'])){
    $row = listNews($_GET['id']);
  }else{
    // echo ('prueba');
    header("Location: http://innovame2/noticias/index.html"); /* Redirect browser */
    exit();
  }
?>


<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Innovame | Ministerio de Educación de Corrientes</title>
  <link href="../css/reset.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/animate.min.css" rel="stylesheet"> 
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/lightbox.css" rel="stylesheet">
  <link href="../css/main.css" rel="stylesheet">
  <link id="css-preset" href="../css/preset.css" rel="stylesheet">
  <link href="../css/responsive.css" rel="stylesheet">

   <style type="text/css">
    .main-nav{
        position: fixed;
        width: 100%;
        z-index: 9999;
    }
  </style>

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="../images/faviconmain.ico">
</head><!--/head-->

<body>

  <!--.preloader-->
  <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
  <!--/.preloader-->


<div class="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">
            <h1><img class="logo img-responsive" src="../images/logo-nav.png" alt="logo"></h1>
          </a>                    
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">                 
            <li class=""><a href="../index.html">Inicio</a></li>
            <li class=" active"><a href="index.html">Noticias</a></li>
            <li class=""><a href="../index.html#areas">Áreas</a></li>
            <li class=""><a href="../index.html#faq">Preguntas Frecuentes</a></li>
            <li class=""><a href="../index.html#contacto">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div><!--/#main-nav-->
  
  <div class="container">

    <div class="new-main">
      <div class="new-img"><a href="#"><img src="<?php echo $row['fotoPath']; ?>" /></a></div>

      <div class="new-content">
      <?php if (isset($row)) { ?>
        <h1 class="new-title"><?php echo $row['titulo']; ?></h1>
          <p class="date"><?php echo $row['fechaSubida']; ?></p>

          <p class="intro"><?php echo $row['resumen']; ?></p>

          <p><?php echo $row['contenido']; ?></p>

      <?php } ?>
      </div>

      <form>
      <div class="form-group">
        <button class="btn-submit" name="Back2" onclick="history.back()">VOLVER</button>
      </div>
        <!-- <div align="center">
          <input type="button" value="VOLVER ATRÁS" name="Back2" onclick="history.back()" />
        </div> -->
      </form>
    </div>
  </div>

  <footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <div class="container text-center">
        <div class="footer-logo">
          <a href="index.html"><img style="float: right; margin: 0 60px;" class="logo-md img-responsive" src="../images/logo-footer.png" alt=""></a>
          <a href="http://www.mec.gob.ar"><img style="float: right; margin: 0 60px;" class="logo-md img-responsive" src="../images/logoMinEd.png" alt=""></a>
        </div>
        <div class="social-icons">
          <ul>
            <li><a class="envelope" href="mailto:innova@mec.gob.ar" target="_blank"><i class="fa fa-envelope"></i></a></li>
            <li><a class="twitter" href="http://www.twitter.com/InnovameCtes" target="_blank"><i class="fa fa-twitter"></i></a></li> 
            <li><a class="facebook" href="http://www.facebook.com/InnovameCtes" target="_blank"><i class="fa fa-facebook"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p>&copy; 2017 Innovame - Ministerio de Educación de Corrientes</p>
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="../js/jquery.inview.min.js"></script>
  <script type="text/javascript" src="../js/wow.min.js"></script>
  <script type="text/javascript" src="../js/mousescroll.js"></script>
  <script type="text/javascript" src="../js/smoothscroll.js"></script>
  <script type="text/javascript" src="../js/jquery.countTo.js"></script>
  <script type="text/javascript" src="../js/lightbox.min.js"></script>
  <script type="text/javascript" src="../js/main.js"></script>
  <script type="text/javascript" src="../js/faq.js"></script>

</body>
</html>