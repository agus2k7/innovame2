<?php

session_start();
 
/**
 * Include ircmaxell's password_compat library.
 */
// require 'lib/password.php';
 
require 'conexion.php'; 
 
if(isset($_POST['login'])){
    $pdo = conectar();
    
    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $passwordAttempt = !empty($_POST['password']) ? trim($_POST['password']) : null;

    $sql = "SELECT id, username, password FROM user WHERE username = :username";
    $stmt = $pdo->prepare($sql);
    
    $stmt->bindValue(':username', $username);
    
    $stmt->execute();
    
    $user = $stmt->fetch(PDO::FETCH_ASSOC); 

    if($user === false){

        // die('usuario y/o password incorrectos');
        echo 'usuario y/o password incorrectos';
    } else{

        // $validPassword = password_verify($passwordAttempt, $user['password']);
        
        // if($validPassword){
      if($user['password'] == $passwordAttempt){
            
            //Provide the user with a login session.
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['logged_in'] = time();
            $_SESSION['username'] = $user['username'];
            echo 'ok';
            
            // header('Location: panel.php');
            // exit;
            
        } else{

            // die('usuario y/o password incorrectos');
            echo ('usuario y/o password incorrectos');
        }
    }
    
} 
?>