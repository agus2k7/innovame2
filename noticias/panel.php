<?php
include('session.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Innovame | Ministerio de Educación de Corrientes</title>
  <link href="../css/reset.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/animate.min.css" rel="stylesheet"> 
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/lightbox.css" rel="stylesheet">
  <link href="../css/main.css" rel="stylesheet">
  <link id="css-preset" href="../css/preset.css" rel="stylesheet">
  <link href="../css/responsive.css" rel="stylesheet">

  <style type="text/css">
    .main-nav{
      position: fixed;
      width: 100%;
      z-index: 9999;
    }
  </style>

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="../images/faviconmain.ico">
  </head><!--/head-->

  <body>

    <!--.preloader-->
    <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
    <!--/.preloader-->


    <div class="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">
            <h1><img class="logo img-responsive" src="../images/logo-nav.png" alt="logo"></h1>
          </a>                    
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">                 
            <li class=""><a href="../index.html">Inicio</a></li>
            <li class="active"><a href="index.html">Noticias</a></li>
            <li class=""><a href="../index.html#areas">Áreas</a></li>
            <li class=""><a href="../index.html#faq">Preguntas Frecuentes</a></li>
            <li class=""><a href="../index.html#contacto">Contacto</a></li>
          </ul>
        </div>
      </div>
    </div><!--/#main-nav-->

    <section>
      <div class="container">
        <div class="new-main">
        <div class="row">
          <!-- <div class="col-sm-12"> -->
            <div class="col-sm-offset-9">
              Bienvenido <?php echo $usuario; ?>
              <a href = "logout.php">Salir</a>
            </div>
          <!-- </div> -->
        </div>
          <div class="new-content">
            <span id="resultado"></span>
            <h1 class="new-title">Agrega una Noticia</h1>
            <form id="noticias-form" enctype="multipart/form-data" name="contact-form" method="post" action="altanoticia.php">
              <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                      <input type="text" name="titulo" id="titulo" class="form-control" placeholder="Título" required="required">
                    </div>
                  </div>
                  <div class="col-sm-9">
                    <div class="form-group">
                      <input type="text" name="resumen" id="resumen" class="form-control" placeholder="Breve resumen de la noticia" required="required">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>
                    <!-- <input type="file" name=""/> -->
                    <input type="file" name="pictures" id="pictures" class="form-control" required="required">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <textarea name="contenido" id="contenido" class="form-control" rows="4" placeholder="Contenido de la Noticia" required="required"></textarea>
              </div>                        
              <div class="form-group">
                <?php echo '<input type="hidden" name="user_id" value="'.$user_id.'">' ?>
                <button name="submit" type="submit" class="btn-submit">Publicar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>


  <footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <div class="container text-center">
        <div class="footer-logo">
          <a href="index.html"><img style="float: right; margin: 0 60px;" class="logo-md img-responsive" src="../images/logo-footer.png" alt=""></a>
          <a href="http://www.mec.gob.ar"><img style="float: right; margin: 0 60px;" class="logo-md img-responsive" src="../images/logoMinEd.png" alt=""></a>
        </div>
        <div class="social-icons">
          <ul>
            <li><a class="envelope" href="mailto:innova@mec.gob.ar" target="_blank"><i class="fa fa-envelope"></i></a></li>
            <li><a class="twitter" href="http://www.twitter.com/InnovameCtes" target="_blank"><i class="fa fa-twitter"></i></a></li> 
            <li><a class="facebook" href="http://www.facebook.com/InnovameCtes" target="_blank"><i class="fa fa-facebook"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p>&copy; 2017 Innovame - Ministerio de Educación de Corrientes</p>
          </div>
          <div class="col-sm-6">

          </div>
        </div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/noticias.js"></script>
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="../js/jquery.inview.min.js"></script>
  <script type="text/javascript" src="../js/wow.min.js"></script>
  <script type="text/javascript" src="../js/mousescroll.js"></script>
  <script type="text/javascript" src="../js/smoothscroll.js"></script>
  <script type="text/javascript" src="../js/jquery.countTo.js"></script>
  <script type="text/javascript" src="../js/lightbox.min.js"></script>
  <script type="text/javascript" src="../js/main.js"></script>
  <script type="text/javascript" src="../js/faq.js"></script>

</body>
</html>